---
layout: home
---

# OpenFlexure Industries is no longer selling microscope kits.

### For the OpenFlexure the Open Source project go to [openflexure.org](https://openflexure.org).

### The company OpenFlexure Industries LTD has become Foxhill Engineering LTD. Foxhill Engineering offers consultancy for those building precision instrumentation or open source hardware. For more information email [julian@foxhill-engineering.co.uk](mailto:julian@foxhill-engineering.co.uk)

